<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http:\\www.kmotors.com
 * @since             1.0.0
 * @package           Kalories
 *
 * @wordpress-plugin
 * Plugin Name:       kalories
 * Plugin URI:        http:\\www.kmotors.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Farzam Tahmaseb mirza
 * Author URI:        http:\\www.kmotors.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       kalories
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


define( 'Kalories', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-kalories-activator.php
 */
function activate_kalories() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kalories-activator.php';
	Kalories_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-kalories-deactivator.php
 */
function deactivate_kalories() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kalories-deactivator.php';
	Kalories_Deactivator::deactivate();
}


register_activation_hook( __FILE__, 'activate_kalories' );
register_deactivation_hook( __FILE__, 'deactivate_kalories' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-kalories.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_kalories() {

	$plugin = new Kalories();
	$plugin->run();

}
run_kalories();

// admin page for managing Max colories of day
require_once plugin_dir_path( __FILE__ ) . 'admin/admin-page.php';

// user page for managing meals and colories
require_once plugin_dir_path( __FILE__ ) . 'admin/manage-meals-page.php';


require_once plugin_dir_path( __FILE__ ) . 'admin/meals-page.php';
