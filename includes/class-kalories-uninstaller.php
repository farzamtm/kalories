<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http:\\www.kmotors.com
 * @since      1.0.0
 *
 * @package    Kalories
 * @subpackage Kalories/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Kalories
 * @subpackage Kalories/includes
 * @author     Farzam Tahmaseb mirza <farzamit@gmail.com>
 */
class Kalories_Unistaller {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function uninstall() {

        if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
            exit();
        }

        delete_option( "kalories_max_colories_of_day");

        global $wpdb;


        $wpdb->query("DROP TABLE IF EXISTS  {$wpdb->prefix}user_meal;");
        $wpdb->query("DROP TABLE IF EXISTS  {$wpdb->prefix}meal;");



    }

}
