<?php

/**
 * Fired during plugin activation
 *
 * @link       http:\\www.kmotors.com
 * @since      1.0.0
 *
 * @package    Kalories
 * @subpackage Kalories/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Kalories
 * @subpackage Kalories/includes
 * @author     Farzam Tahmaseb mirza <farzamit@gmail.com>
 */
class Kalories_Activator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {

        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE `{$wpdb->base_prefix}meals` (
          `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
          `date` date NOT NULL,
          `meal_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
          `number_of_colories` int(11) NOT NULL,
          `user_id` bigint(20) UNSIGNED NOT NULL
        ) ENGINE=InnoDB  $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        add_option("kalories_max_colories_of_day", "1000", '', true);
    }

}
