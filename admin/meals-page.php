<?php
/*
 * Plugin Name:       kalories
 * Plugin URI:        http:\\www.kmotors.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Farzam Tahmaseb mirza
 * Author URI:        http:\\www.kmotors.com
*/


// display form
function meals_form_display()
{

    $meal_name="";
    $number_of_colories="";

    if(isset($_GET['edit_id'])){
        global $wpdb;

        $user_id = get_current_user_id();

        $edit_meals =  $wpdb->get_results( $wpdb->prepare(
            "select meals.id,meals.meal_name,meals.number_of_colories,meals.date from {$wpdb->base_prefix}meals meals  join {$wpdb->base_prefix}users users on(meals.user_id=users.id) where users.id=" . $user_id . " and meals.id= %d ",
            $_GET['edit_id']
        ) );

        $meal_name = $edit_meals[0]->meal_name;
        $number_of_colories = $edit_meals[0]->number_of_colories;

    }

    ?>

    <form method="post">
        <p><label for="meal_name">Meal name</label></p>
        <p><input id="meal_name" value="<?=$meal_name;?>" name="meal_name"></p>
        <p><label for="number_of_colories">number of colories</label></p>
        <p><input id="number_of_colories" value="<?=$number_of_colories;?>" name="number_of_colories"></p>
        <?php if(isset($_GET['edit_id'])){ ?>
            <p><input type="submit" name="edit" class="button button-primary" value="update"></p>
        <?php } else { ?>
             <p><input type="submit" name="insert" class="button button-primary" value="submit"></p>
        <?php } ?>
        <a><a href="<?= menu_page_url('manage-meals-kolories', false) ?>"  class="button button-primary"  >return</a>
    </form>

    <?php

}


// process submitted form
function meals_form_process()
{

    global $wpdb;

    $user_id = get_current_user_id();

    if(isset($_POST['insert'])){

        if(!$_POST['meal_name']){
            echo '<p style="color: red">Meal name is required</p>';
            return;
        }

        if(!$_POST['number_of_colories']){
            echo '<p style="color: red">number of colories is required</p>';
            return;
        }

        if(!is_numeric($_POST['number_of_colories'])){
            echo '<p style="color: red">number of colories must be number</p>';
            return;
        }



        $wpdb->query( $wpdb->prepare(
            "insert into {$wpdb->base_prefix}meals (meal_name,number_of_colories,date,user_id)  values (%s , %d , now(), %d)",
            $_POST['meal_name'],
            $_POST['number_of_colories'],
            $user_id
        ) );

        wp_redirect('manage-meals-kolorie');

    }

    if(isset($_POST['edit'])){

        if(!$_POST['meal_name']){
            echo '<p style="color: red">Meal name is required</p>';
            return;
        }

        if(!$_POST['number_of_colories']){
            echo '<p style="color: red">number of colories is required</p>';
            return;
        }

        if(!$_GET['edit_id']){
            echo '<p style="color: red">edit_id is required</p>';
            return;
        }

        if(!is_numeric($_POST['number_of_colories'])){
            echo '<p style="color: red">number of colories must be number</p>';
            return;
        }

        $wpdb->query( $wpdb->prepare(
            "update {$wpdb->base_prefix}meals set meal_name = %s,number_of_colories= %d, date= now() where user_id = %d and id= %d",
            $_POST['meal_name'],
            $_POST['number_of_colories'],
            $user_id,
            $_GET['edit_id']
        ) );

        echo "<p style='color:green;'>meal is updated successfuly</p>";


    }

}


/*

	Adding the plugin menu and settings page
	Below this line covered later in the course
	See video: 3.02 - Adding administrative menus
	Ignore this stuff for now..

*/

// add top-level administrative menu
function add_create_edit_meal_menu()
{

    add_menu_page(
        'add Meals',
        'add Meals',
        'subscriber',
        'add-meals-kolories',
        'create_edit_meals',
        'dashicons-admin-generic',
        null
    );


}


add_action('admin_menu', 'add_create_edit_meal_menu');



// display the plugin settings page
function create_edit_meals()
{

    global $current_user; // Use global

    // check if user is allowed access
    if (! user_can( $current_user, "subscriber" ) ) return;

    ?>

    <div class="wrap">

        <h1><?php echo esc_html(get_admin_page_title()); ?></h1>

        <?php meals_form_process(); ?>
        <?php meals_form_display(); ?>


    </div>

    <?php

}


