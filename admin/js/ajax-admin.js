/*
	
	Ajax Example - JavaScript for Admin Area
	
*/
(function($) {
	
	$(document).ready(function() {
		
		// when user submits the form
		$(".search_form").on( 'submit', function(event) {

            var meal_name = $("#meal_name").val();
            var from_date = $("#from_date").val();
            var to_date = $("#to_date").val();

			// prevent form submission
			event.preventDefault();
			
			// add loading message
			$('#meals_table').html('Loading...');
			
			// submit the data
			$.post(ajaxurl, {
				
				nonce:  ajax_admin.nonce,
				action: 'admin_hook',
                meal_name:    meal_name,
                from_date:    from_date,
                to_date:    to_date

			}, function(data) {

                $('#meals_table').html("");
                $('#meals_table').html(data);

			});
			
		});
		
	});
	
})( jQuery );
