<?php
/*
 * Plugin Name:       kalories
 * Plugin URI:        http:\\www.kmotors.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Farzam Tahmaseb mirza
 * Author URI:        http:\\www.kmotors.com
*/


function process_meals_delete_form()
{


    if (isset($_POST['delete'])) {

        global $wpdb;

        $user_id = get_current_user_id();

        $wpdb->query($wpdb->prepare(
            "delete from {$wpdb->base_prefix}meals where id= %d and user_id=%d",
            $_POST['meal_id'],
            $user_id
        ));

    }
}

// display form
function manage_meals_form_display()
{

    global $wpdb;

    $user_id = get_current_user_id();

    $meals = $wpdb->get_results("select meals.id,meals.meal_name,meals.number_of_colories,meals.date from {$wpdb->base_prefix}meals meals join {$wpdb->base_prefix}users users on(meals.user_id=users.id) where users.id=" . $user_id . " order by meals.id desc");

    $meals_total_colories = $wpdb->get_results("select sum(meals.number_of_colories) as total from {$wpdb->base_prefix}meals meals  join {$wpdb->base_prefix}users users on(meals.user_id=users.id) where users.id=" . $user_id . "  and meals.date = '" . date('Y-m-d') . "' group by meals.date");

    $today_total = $meals_total_colories[0]->total;

    ?>

    <div class="ajax-form-wrap">
        <form method="post" class="search_form">
            <p><label>Meal Name</label><input name="meal_name" id="meal_name"></p>
            <p><label>from Meal Date</label><input type="date" name="from_date" id="from_date"></p>
            <p><label>to Meal Date</label><input type="date" name="to_date" id="to_date"></p>
            <p><input type="submit" class="button button-primary" value="search"></p>
        </form>
    </div>

    <table class="widefat fixed" cellspacing="0">
        <thead>
        <th>#</th>
        <th>name</th>
        <th>number of colories</th>
        <th>date</th>
        <th>tools</th>
        </thead>
        <tbody id="meals_table">
        <?php
        foreach ($meals as $key => $meal) {

            echo "<tr>
                        <td>" . ($key + 1) . "</td>
                        <td>" . $meal->meal_name . "</td>
                        <td>" . $meal->number_of_colories . "</td>
                        <td>" . $meal->date . "</td>
                        <td>
                            <a href='" . menu_page_url('add-meals-kolories', false) . "&edit_id=" . $meal->id . "' class='button button-primary'>edit</a>
                            <form method='post'>
                                <input type='hidden' name='meal_id' value='" . $meal->id . "' >
                                <input type='submit' name='delete' value='delete' class='button button-danger'>
                            </form>
                            
                        </td>
                    </tr>";
        }
        ?>
        </tbody>


    </table>

    <?php if ($today_total >= get_option("kalories_max_colories_of_day")) { ?>
    <p style="color:green">today total colories: <?= $today_total ?></p>
<?php } else { ?>
    <p style="color:red">today total colories: <?= $today_total ?></p>
<?php } ?>

    <p><a href="<?= menu_page_url('add-meals-kolories', false) ?>" class='button button-success'>add new Meal</a></p>

    <?php
}


/*

	Adding the plugin menu and settings page
	Below this line covered later in the course
	See video: 3.02 - Adding administrative menus
	Ignore this stuff for now..

*/

// add top-level administrative menu
function user_meal_manage()
{

    add_menu_page(
        'Manage Meals',
        'Manage Meals',
        'subscriber',
        'manage-meals-kolories',
        'manage_meals',
        'dashicons-admin-generic',
        null
    );


}


add_action('admin_menu', 'user_meal_manage');

// enqueue scripts
function ajax_admin_enqueue_scripts()
{
    // define script url
    $script_url = plugins_url('/js/ajax-admin.js', __FILE__);

    // enqueue script
    wp_enqueue_script('ajax-admin', $script_url, array('jquery'));


    // create nonce
    $nonce = wp_create_nonce('ajax_admin');

    // define script
    $script = array('nonce' => $nonce);

    // localize script
    wp_localize_script('ajax-admin', 'ajax_admin', $script);

}

add_action('admin_enqueue_scripts', 'ajax_admin_enqueue_scripts');


// process ajax request
function ajax_admin_handler()
{

    global $wpdb;

    check_ajax_referer('ajax_admin', 'nonce');

    $user_id = get_current_user_id();

    $where = "";
    $values = [];

    if ($_POST['meal_name']) {
        $where .= " and meals.meal_name like %s";
        $values[] = '%' . $_POST['meal_name'] . '%';
    }

    if ($_POST['from_date']) {
        $where .= " and meals.date >= %s ";
        $values[] = $_POST['from_date'];
    }

    if ($_POST['to_date']) {
        $where .= " and meals.date <= %s ";
        $values[] = $_POST['to_date'];
    }


    $meals = $wpdb->get_results($wpdb->prepare(
        "select meals.id,meals.meal_name,meals.number_of_colories,meals.date from {$wpdb->base_prefix}meals meals join  {$wpdb->base_prefix}users users on(u_meals.user_id=users.id) where users.id=" . $user_id . $where . " order by meals.id desc",
        $values
    ));

    foreach ($meals as $key => $meal) {

        echo "<tr>
                        <td>" . ($key + 1) . "</td>
                        <td>" . $meal->meal_name . "</td>
                        <td>" . $meal->number_of_colories . "</td>
                        <td>" . $meal->date . "</td>
                        <td>
                            <a href='" . menu_page_url('add-meals-kolories', false) . "&edit_id='" . $meal->id . "' class='button button-primary'>edit</a>
                            <form method='post'>
                                <input type='hidden' name='meal_id' value='" . $meal->id . "' >delete</input>
                                <input type='submit' name='delete' class='button button-danger'>delete</input>
                            </form>
                            
                        </td>
                    </tr>";
    }

    // end processing

    wp_die();

}

// ajax hook for logged-in users: wp_ajax_{action}
add_action('wp_ajax_admin_hook', 'ajax_admin_handler');
add_action('p_ajax_nopriv_admin_hook', 'ajax_admin_handler');


// display the plugin settings page
function manage_meals()
{

    global $current_user; // Use global

    // check if user is allowed access
    if (!user_can($current_user, "subscriber")) return;
    ?>

    <div class="wrap">

        <h1><?php echo esc_html(get_admin_page_title()); ?></h1>

        <?php process_meals_delete_form(); ?>
        <?php manage_meals_form_display(); ?>


    </div>

    <?php

}


