<?php
/*
 * Plugin Name:       kalories
 * Plugin URI:        http:\\www.kmotors.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Farzam Tahmaseb mirza
 * Author URI:        http:\\www.kmotors.com
*/


// display form
function max_colories_display_form()
{

    ?>

    <form method="post">
        <p><label for="colories">Please enter Max colories of a day</label></p>
        <p><input id="max_colories" value="<?= get_option("kalories_max_colories_of_day")?>" name="kalories_max_colories_of_day"></p>
        <p><input type="submit" value="update data"></p>
    </form>

    <?php

}


// process submitted form
function max_colories_process_form()
{

    if (isset($_POST['kalories_max_colories_of_day'])) {

        $colories = $_POST['kalories_max_colories_of_day'];

        if (is_numeric($colories)) {

            update_option("kalories_max_colories_of_day", $colories, true);

            echo '<p style="color: green">max Colories was updated Successfully!</p>';


        } else {

            echo '<p>Please enter a number!</p>';

        }

    }

}


/*

	Adding the plugin menu and settings page
	Below this line covered later in the course
	See video: 3.02 - Adding administrative menus
	Ignore this stuff for now..

*/

// add top-level administrative menu
function max_colories_settings()
{

    add_submenu_page(
        'options-general.php',
        'Kalories Settings',
        'Kalories',
        'manage_options',
        'kalories-settings',
        'max_colories_of_a_day'
    );


}

add_action('admin_menu', 'max_colories_settings');


// display the plugin settings page
function max_colories_of_a_day()
{

    // check if user is allowed access
    if (!current_user_can('manage_options')) return;

    ?>

    <div class="wrap">

        <h1><?php echo esc_html(get_admin_page_title()); ?></h1>

        <?php max_colories_process_form(); ?>
        <?php max_colories_display_form(); ?>


    </div>

    <?php

}


